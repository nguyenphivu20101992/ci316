<?php
namespace CI316\libraries;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Reposnse Class
 * A fully RESTful server implementation for CodeIgniter using one library, one config file and one controller.
 *
 * @category        Libraries
 * @version         3.0.0
 * @package         CodeIgniter
 * @subpackage      Libraries
 *
 * @license         MIT
 */
class Response extends \Symfony\Component\HttpFoundation\JsonResponse
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setCORS($allow = true)
    {
        if (!$allow) {
            return;
        }

        $this->headers->set('Access-Control-Allow-Origin', '*');
        $this->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Access-Token, If-None-Match');
        $this->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS, PATCH, DELETE');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $this->headers->set('Access-Control-Allow-Origin', $_SERVER['HTTP_ORIGIN']);
            $this->headers->set('Access-Control-Allow-Credentials', 'true');
        }
        // Access-Control headers are received during OPTIONS requests
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                $this->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS, PATCH, DELETE');
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                $this->headers->set('Access-Control-Allow-Headers', $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
            }
            exit(0);
        }
    }

    /**
     * @param $data
     * @param null        $httpCode
     * @param selfHTTP_OK $message
     */
    public function response($data = null, $httpCode = self::HTTP_OK, $message = '')
    {
        $this->setCORS();
        $this->setStatusCode($httpCode);
        $messageCode = [
            'code' => $httpCode,
            'message' => $message,
        ];

        $this->statusText = $message;

        if ($data && (is_array($data) || is_object($data))) {
            $data = array_merge($messageCode, $data);
        }

        if ($data === null) {
            $data = $messageCode;
        }
        $this->setData($data)->send();
    }
}
