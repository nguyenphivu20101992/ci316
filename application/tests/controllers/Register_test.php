<?php
class Register_test extends TestCase
{
    public function setUP()
    {
        $this->resetInstance();
        $rows = [
            'email'     => 'admin@neo-lab.vn',
            'password'  => md5('admin'),
            'address'   => 'Da Nang',
            'username'  => 'Phi Vu Nguyen',
            'phone'     => '01225584121',
            'role_id'   => 1
        ];
        $this->CI->db->truncate('accounts');
        $this->CI->db->insert('accounts',$rows);
    }
    public function testShowRegister()
	{
        $output = $this->request('GET', '/register');
        $this->assertResponseCode(200);
        $this->assertContains('<form action="'.base_url().'register" method="post" accept-charset="utf-8">', $output);
    }
    public function testRegisterValidEmailError()
    {
        $data=[
            'email'     =>  'admin@neo-lab',
        ];
        $reports = 'Email must be in the correct format abc@xyz.jk';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testRegisterEmailBlank()
    {
        $data=[
            'email'     =>  '',
        ];
        $reports = 'You must enter Email.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testRegisterEmailUnique()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
        ];
        $reports = 'The Email field does not have our email.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
        
    }

    public function testRegisterValidPassError()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asc',
        ];
        $reports = 'Password minlenght 5 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
        
    }

    public function testRegisterPassBlank()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  ''
        ];
        $reports = 'You must enter Password.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testRegisterValidAddressError()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asdasdad',
            'address'  =>  'asc',
        ];
        $reports = 'Address minlenght 5 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testAddressBlank()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asdasdad',
            'address'     =>  '',
        ];
        $reports = 'You must enter Address.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testMaxLengthPass()
    { 
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quisquam neque a autem, sit beatae exercitationem deleniti facere adipisci veritatis nulla voluptate, placeat laudantium ullam. Cumque ducimus, dolores ut vitae adipisci necessitatibus rem tempore hic blanditiis est molestias quia, molestiae esse debitis quod quam quas consectetur error officia tenetur. Officiis natus tempore fugiat necessitatibus nulla facilis quia nemo, quaerat distinctio repellat fugit eius labore veritatis beatae ullam laborum, dolorum dolore? Repellendus neque perferendis earum? Nobis excepturi tenetur natus, aut quae optio repellat ut enim ratione obcaecati amet possimus deleniti maxime qui labore sunt reprehenderit neque voluptate nam est iure animi cumque. Porro minima distinctio natus quis aperiam fuga, omnis, quod aliquam rerum ipsam aliquid. Recusandae corporis deserunt sit ratione quo iusto necessitatibus impedit, asperiores illum ipsa, laborum, natus neque. Libero cupiditate qui repudiandae eligendi eius quas mollitia iusto earum officia. Distinctio similique veritatis nisi, id sequi eius voluptate cupiditate libero cumque laborum quibusdam unde rem possimus. Minus numquam quisquam delectus asperiores? Numquam nobis commodi in temporibus blanditiis esse laudantium explicabo sint, eum mollitia at molestiae ea incidunt harum? Ea porro ipsum sequi placeat quo. Eos accusantium laborum, dolorum fuga culpa ratione mollitia dicta corrupti nihil quo est sunt voluptas, reprehenderit alias cupiditate ipsam, quidem autem? Voluptatem, nostrum dolorem. Doloribus totam enim magni repellendus quos laboriosam, rerum sunt quidem temporibus explicabo molestiae, nobis aut debitis saepe dolore modi! Magni optio atque quasi perferendis blanditiis vitae placeat odit, nesciunt quibusdam corrupti fuga similique ducimus natus? Cupiditate et alias facere debitis unde? Delectus distinctio hic sint ut praesentium in eos ratione consectetur aperiam cumque magni, vitae dolore nesciunt, porro, expedita similique. Numquam odio ipsum explicabo quos quia corrupti impedit accusantium itaque necessitatibus optio! Cum voluptates doloribus sequi, provident repellendus commodi harum aliquid facilis culpa tenetur quisquam magni aspernatur eos excepturi, nesciunt numquam delectus.',
            'address'   =>  'Da nang',
            'username'  =>  'Phi Vu nguyen',
            'phone'     =>  'assasdadsad'
        ];
        $reports = 'Password maxlenght 255 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testRegisterValidUserNameError()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asdasdad',
            'address'   =>  'Da nang city',
            'username'  =>  'as'
        ];
        $reports = 'UserName minlenght 5 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testUserNameBlank()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asdasdad',
            'address'   =>  'Da nang',
            'username'  =>  ''
        ];
        $reports = 'You must enter UserName.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testMaxLengthAddress()
    { 
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'address'  =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quisquam neque a autem, sit beatae exercitationem deleniti facere adipisci veritatis nulla voluptate, placeat laudantium ullam. Cumque ducimus, dolores ut vitae adipisci necessitatibus rem tempore hic blanditiis est molestias quia, molestiae esse debitis quod quam quas consectetur error officia tenetur. Officiis natus tempore fugiat necessitatibus nulla facilis quia nemo, quaerat distinctio repellat fugit eius labore veritatis beatae ullam laborum, dolorum dolore? Repellendus neque perferendis earum? Nobis excepturi tenetur natus, aut quae optio repellat ut enim ratione obcaecati amet possimus deleniti maxime qui labore sunt reprehenderit neque voluptate nam est iure animi cumque. Porro minima distinctio natus quis aperiam fuga, omnis, quod aliquam rerum ipsam aliquid. Recusandae corporis deserunt sit ratione quo iusto necessitatibus impedit, asperiores illum ipsa, laborum, natus neque. Libero cupiditate qui repudiandae eligendi eius quas mollitia iusto earum officia. Distinctio similique veritatis nisi, id sequi eius voluptate cupiditate libero cumque laborum quibusdam unde rem possimus. Minus numquam quisquam delectus asperiores? Numquam nobis commodi in temporibus blanditiis esse laudantium explicabo sint, eum mollitia at molestiae ea incidunt harum? Ea porro ipsum sequi placeat quo. Eos accusantium laborum, dolorum fuga culpa ratione mollitia dicta corrupti nihil quo est sunt voluptas, reprehenderit alias cupiditate ipsam, quidem autem? Voluptatem, nostrum dolorem. Doloribus totam enim magni repellendus quos laboriosam, rerum sunt quidem temporibus explicabo molestiae, nobis aut debitis saepe dolore modi! Magni optio atque quasi perferendis blanditiis vitae placeat odit, nesciunt quibusdam corrupti fuga similique ducimus natus? Cupiditate et alias facere debitis unde? Delectus distinctio hic sint ut praesentium in eos ratione consectetur aperiam cumque magni, vitae dolore nesciunt, porro, expedita similique. Numquam odio ipsum explicabo quos quia corrupti impedit accusantium itaque necessitatibus optio! Cum voluptates doloribus sequi, provident repellendus commodi harum aliquid facilis culpa tenetur quisquam magni aspernatur eos excepturi, nesciunt numquam delectus.',
            'password'   =>  'Da nang',
            'username'  =>  'Phi Vu nguyen',
            'phone'     =>  '12312312313123'
        ];
        $reports = 'Address maxlenght 255 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testRegisterValidPhoneError()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asdasdad',
            'address'   =>  'Da nang city',
            'username'  =>  'Phi Vu nguyen',
            'phone'     =>  '12'
        ];
        $reports = 'Phone minlenght 10 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testPhoneBlank()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asdasdad',
            'address'   =>  'Da nang',
            'username'  =>  'Phi Vu nguyen',
            'phone'     =>  ''
        ];
        $reports = 'You must enter Phone.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testMaxLengthUserName()
    { 
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'username'  =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quisquam neque a autem, sit beatae exercitationem deleniti facere adipisci veritatis nulla voluptate, placeat laudantium ullam. Cumque ducimus, dolores ut vitae adipisci necessitatibus rem tempore hic blanditiis est molestias quia, molestiae esse debitis quod quam quas consectetur error officia tenetur. Officiis natus tempore fugiat necessitatibus nulla facilis quia nemo, quaerat distinctio repellat fugit eius labore veritatis beatae ullam laborum, dolorum dolore? Repellendus neque perferendis earum? Nobis excepturi tenetur natus, aut quae optio repellat ut enim ratione obcaecati amet possimus deleniti maxime qui labore sunt reprehenderit neque voluptate nam est iure animi cumque. Porro minima distinctio natus quis aperiam fuga, omnis, quod aliquam rerum ipsam aliquid. Recusandae corporis deserunt sit ratione quo iusto necessitatibus impedit, asperiores illum ipsa, laborum, natus neque. Libero cupiditate qui repudiandae eligendi eius quas mollitia iusto earum officia. Distinctio similique veritatis nisi, id sequi eius voluptate cupiditate libero cumque laborum quibusdam unde rem possimus. Minus numquam quisquam delectus asperiores? Numquam nobis commodi in temporibus blanditiis esse laudantium explicabo sint, eum mollitia at molestiae ea incidunt harum? Ea porro ipsum sequi placeat quo. Eos accusantium laborum, dolorum fuga culpa ratione mollitia dicta corrupti nihil quo est sunt voluptas, reprehenderit alias cupiditate ipsam, quidem autem? Voluptatem, nostrum dolorem. Doloribus totam enim magni repellendus quos laboriosam, rerum sunt quidem temporibus explicabo molestiae, nobis aut debitis saepe dolore modi! Magni optio atque quasi perferendis blanditiis vitae placeat odit, nesciunt quibusdam corrupti fuga similique ducimus natus? Cupiditate et alias facere debitis unde? Delectus distinctio hic sint ut praesentium in eos ratione consectetur aperiam cumque magni, vitae dolore nesciunt, porro, expedita similique. Numquam odio ipsum explicabo quos quia corrupti impedit accusantium itaque necessitatibus optio! Cum voluptates doloribus sequi, provident repellendus commodi harum aliquid facilis culpa tenetur quisquam magni aspernatur eos excepturi, nesciunt numquam delectus.',
            'password'   =>  'Da nang',
            'address'  =>  'Phi Vu nguyen',
            'phone'     =>  '1231231231231'
        ];
        $reports = 'UserName maxlenght 255 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testPhoneNumberic()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asdasdad',
            'address'   =>  'Da nang',
            'username'  =>  'Phi Vu nguyen',
            'phone'     =>  'assasdadsad'
        ];
        $reports = 'Phone enter the numberic';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testMaxLengthPhone()
    { 
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'phone'  =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quisquam neque a autem, sit beatae exercitationem deleniti facere adipisci veritatis nulla voluptate, placeat laudantium ullam. Cumque ducimus, dolores ut vitae adipisci necessitatibus rem tempore hic blanditiis est molestias quia, molestiae esse debitis quod quam quas consectetur error officia tenetur. Officiis natus tempore fugiat necessitatibus nulla facilis quia nemo, quaerat distinctio repellat fugit eius labore veritatis beatae ullam laborum, dolorum dolore? Repellendus neque perferendis earum? Nobis excepturi tenetur natus, aut quae optio repellat ut enim ratione obcaecati amet possimus deleniti maxime qui labore sunt reprehenderit neque voluptate nam est iure animi cumque. Porro minima distinctio natus quis aperiam fuga, omnis, quod aliquam rerum ipsam aliquid. Recusandae corporis deserunt sit ratione quo iusto necessitatibus impedit, asperiores illum ipsa, laborum, natus neque. Libero cupiditate qui repudiandae eligendi eius quas mollitia iusto earum officia. Distinctio similique veritatis nisi, id sequi eius voluptate cupiditate libero cumque laborum quibusdam unde rem possimus. Minus numquam quisquam delectus asperiores? Numquam nobis commodi in temporibus blanditiis esse laudantium explicabo sint, eum mollitia at molestiae ea incidunt harum? Ea porro ipsum sequi placeat quo. Eos accusantium laborum, dolorum fuga culpa ratione mollitia dicta corrupti nihil quo est sunt voluptas, reprehenderit alias cupiditate ipsam, quidem autem? Voluptatem, nostrum dolorem. Doloribus totam enim magni repellendus quos laboriosam, rerum sunt quidem temporibus explicabo molestiae, nobis aut debitis saepe dolore modi! Magni optio atque quasi perferendis blanditiis vitae placeat odit, nesciunt quibusdam corrupti fuga similique ducimus natus? Cupiditate et alias facere debitis unde? Delectus distinctio hic sint ut praesentium in eos ratione consectetur aperiam cumque magni, vitae dolore nesciunt, porro, expedita similique. Numquam odio ipsum explicabo quos quia corrupti impedit accusantium itaque necessitatibus optio! Cum voluptates doloribus sequi, provident repellendus commodi harum aliquid facilis culpa tenetur quisquam magni aspernatur eos excepturi, nesciunt numquam delectus.',
            'password'   =>  'Da nang',
            'address'  =>  'Phi Vu nguyen',
            'username'     =>  '1231231231231'
        ];
        $reports = 'Phone maxlenght 255 character.';
        $output = $this->request('POST', '/register', $data);
        $this->assertContains($reports, $output);
    }
    public function testRegisterSuccess()
    {
        $data=[
            'email'     => 'vu.np@neo-lab.vn',
            'password'  => md5('admin'),
            'address'   => 'Da Nang',
            'username'  => 'Phi Vu Nguyen',
            'phone'     => '01225584121',
            ];
        $output = $this->request('POST', '/register', $data);
        $reports = 'Regiset account success';;
        $this->assertContains($reports, $output);
    }
}
