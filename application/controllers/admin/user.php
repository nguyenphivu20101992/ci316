<?php 
use CI316\core\BaseController;
class User extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
	}
	/**
	 * Show admin index
	 */
	public function indexAdmin(){
		$this->load->templateAdmin('/admin/index_admin');
	}
}