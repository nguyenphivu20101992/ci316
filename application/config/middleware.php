<?php

$config['middleware'] = [
    'admin' => 'CI316\middlewares\Admin',
];

$config['middlewareGroups'] = [
    'admin' => [
        'admin'
    ],
    'manager' => [
    ],
];
