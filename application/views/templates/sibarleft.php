<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2 bg-faded sidebar-style-1">
    <h1 class="site-title"><a href="index.html"><em class="fa fa-rocket"></em> Brand.name</a></h1>
    
    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>
    
    <ul class="nav nav-pills flex-column sidebar-nav">
        <li class="nav-item"><a class="nav-link" href="/home"><em class="fa fa-dashboard"></em> Dashboard <span class="sr-only">(current)</span></a></li>
        <?php 
        if(isset($_SESSION['email'])&& $_SESSION['role_name']=='Admin'){
            echo "<li class='nav-item'><a class='nav-link' href='/account'><em class='fa fa-calendar-o'></em> Account Manager</a></li>";
        }
        ?>
        
        
        <!-- <li class="nav-item"><a class="nav-link" href="charts.html"><em class="fa fa-bar-chart"></em> Charts</a></li>
        <li class="nav-item"><a class="nav-link" href="elements.html"><em class="fa fa-hand-o-up"></em> UI Elements</a></li>
        <li class="nav-item"><a class="nav-link" href="cards.html"><em class="fa fa-clone"></em> Cards</a></li> -->
    </ul>
    
    <!-- <a href="/logout" class="logout-button"><em class="fa fa-power-off"></em> Signout</a> -->
</nav>

<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
    <header class="page-header row justify-center">
        <div class="col-md-6 col-lg-8" >
            <h1 class="float-left text-center text-md-left">Dashboard</h1>
        </div>
        <?php //if($_SESSION['id']==$data->id){echo "kkkkkkk";}?>
        <div class="dropdown user-dropdown col-md-6 col-lg-4 text-center text-md-right"><a class="btn btn-stripped dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">        
        <div class="username mt-1">
            <h4 class="mb-1"><?php echo $_SESSION['username'];?></h4>
            
            <h6 class="text-muted"><?php echo $_SESSION['email'];?></h6>
        </div>
        </a>
        
        <div class="dropdown-menu dropdown-menu-right" style="margin-right: 1.5rem;" aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="/profile"><em class="fa fa-user-circle mr-1"></em>Profile</a>
             <!-- <a class="dropdown-item" href="#"><em class="fa fa-sliders mr-1"></em> Preferences</a> -->
             <a class="dropdown-item" href="/logout"><em class="fa fa-power-off mr-1"></em> Logout</a></div>
    </div>
        
        <div class="clear"></div>
    </header>
    <section class="row">
	    <div class="col-sm-12">