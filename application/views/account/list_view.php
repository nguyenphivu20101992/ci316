<div>
<?php echo form_open(base_url().('search')); ?>
    <div class="form-group">
    <small id="helpId" class="form-text text-muted"><h4>Search account</h4></small>
      <input type="text" class="form-control" name="search" id="" aria-describedby="helpId" placeholder="" value="<?php echo(set_value('search')); ?>" required>
      <?php  echo form_error('search', '<p class=" alert-danger">', '</p>');?>
    </div>
    <label class="custom-control custom-radio">
        <input <?php echo  set_radio('radio', '1', TRUE); ?>  name="radio" type="radio" value=1 class="custom-control-input" >
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description">Email</span>
    </label>
    <label class="custom-control custom-radio">
        <input <?php echo  set_radio('radio', '0'); ?>  name="radio" type="radio" class="custom-control-input" value=0>
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description">Full Name</span>
    </label>
    <button type="submit" class="btn btn-primary">Search</button>
    <?php  echo form_error('radio', '<p class=" alert-danger">', '</p>');?>
    <a name="" id="" class="btn info" href="/account"  role="button"><b>Reload</b></a>
</form><br>
</div>
<div class="table-responsive">
    <table class="table">
        <thead>
            <th>#</th>
            <th>Email</th>
            <th>Address</th>
            <th>Full Name</th>
            <th>Phone</th>
            <th>Role</th>
            <th>Edit</th>
            <th>Delete</th>
        </thead>
        <?php 
        $stt=1;
        if(isset($data)){
        foreach ($data as $value) {
            
        ?>
        
        <tr>
            <td><?php echo $stt;?></td>
            <td><?php echo $value['email'];?></td>
            <td><?php echo $value['address'];?></td>
            <td><?php echo $value['username'];?></td>
            <td><?php echo $value['phone'];?></td>
            <td><?php echo $value['role_name'];?></td>
            <td>
                <?php echo form_open(base_url().('showAccountEdit')); ?>
                    <input type="hidden" name="accid"
                        value="<?php echo $value['accid']; ?>">
                    <input type="submit" value="Edit" class="btn btn-info">
                </form>
            </td>
            <td>
                <?php echo form_open(base_url().('showAccountDelete')); ?>
                    <input type="hidden" name="accid"
                        value="<?php echo $value['accid']; ?>">
                    <input type="submit" value="Delete" class="btn btn-warning">
                </form>
            </td>
            
        </tr>
        <?php 
            $stt++;
            }
        }
        ?>
    </table>
</div>
