<?php echo form_open(base_url().('editAccount')); ?>
<input type="hidden" name="accid" value="<?php if(isset($data->accid)) echo($data->accid); ?>">
<input type="hidden" name="role" value="<?php if(isset($data->role_id)) echo($data->role_id); ?>">
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input  readonly="readonly" type="email" name='email' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo(isset($data->email) ? $data->email: set_value('email')); ?>" required>
        <?php  echo form_error('email', '<p class=" alert-danger">', '</p>');?>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Address</label>
        <input type="text" name='address' class="form-control" placeholder="Enter your address" value="<?php echo(isset($data->address) ? $data->address: set_value('address')); ?>" required><?php  echo form_error('address', '<p class=" alert-danger">', '</p>');?>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">User Name</label>
        <input type="text" name='username' class="form-control" placeholder="Enter your username" value="<?php echo(isset($data->username) ? $data->username: set_value('username')); ?>" required>
        <?php  echo form_error('username', '<p class=" alert-danger">', '</p>');?>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Phone</label>
        <input type="text" name='phone' class="form-control" placeholder="Enter your phone" value="<?php echo(isset($data->phone) ? $data->phone: set_value('phone')); ?>" required>
        <?php  echo form_error('phone', '<p class=" alert-danger">', '</p>');?>
    </div>
    <?php if($this->session->flashdata('reportE')){
        echo "<div class='alert alert-warning' ><p>".$this->session->flashdata('report')."</p></div>";}
        if($this->session->flashdata('reportS')){
        echo "<div class='alert alert-success' ><p>".$this->session->flashdata('reportS')."</p></div>";} ?>
    <a name="" id="" class="btn btn-success" href="/account" role="button">Cancer</a>
    <button type="submit" name='submit' value='submit' class=" btn btn-info float-right">Submit</button>
</form>
