<?php
class Maccount extends CI_Model
{
    protected $_table='accounts';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    /*
    *Show all account
    * 
    */
    public function listAccount()
    {
        $query=$this->db->select('accounts.id as accid, accounts.email, accounts.address, accounts.username, accounts.phone,roles.role_name ')
        ->join('roles','roles.id=accounts.role_id')
        ->where('accounts.deleted=')
        ->order_by('accounts.id', 'ASC')
        ->get('accounts');
        
        return $query->result_array();
    }
    /*
    *Get all role
    * 
    */
    public function listRoles()
    {
        $query=$this->db->select('*')->get('roles');
        return $query->result_array();
    }
    /*
    *Get account by id(int)
    * 
    */
    public function getAccountByID($id){
        if(isset($id)){
            $query=$this->db->select('accounts.id as accid, accounts.email, accounts.address, accounts.username, accounts.phone,roles.role_name,accounts.role_id ')
            ->join('roles','roles.id=accounts.role_id')
            ->where('accounts.id=',$id)
            ->get($this->_table);
            return $query->row();
        }else{
            return FALSE;
        }
    }
    /*
    *Update account with id and data update
    * 
    */
    public function updateAccount($id,$data)
    {
        $query=$this->db->select('*')
        ->where('accounts.id=',$id)
        ->where('accounts.deleted=')
        ->get('accounts');
        if($query->num_rows()!=0){
            $this->db->where('accounts.id=',$id)
            ->update('accounts', $data);
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    /*
    *delete account by id(int)
    * 
    */
    public function deleteAccountByID($id){
        $query=$this->db->select('*')
        ->where('accounts.id=',$id)
        ->where('accounts.deleted=')
        ->get('accounts');
        if($query->num_rows()!=0){
            $data=[
                'deleted'   => date('Y-m-d H:i:s')
            ];
            $this->db->where('id=', $id);
            $this->db->update('accounts', $data);
            return TRUE;
        }else{
            return FALSE;
        }
    }
    /*
    *Insert account with email,pass,address,username,phone
    * 
    */
    public function insertAccount($email,$pass,$address,$username,$phone)
    {
        if(isset($email)&&isset($pass)&&isset($address)&&isset($username)&&isset($phone)){
            $data = array(
                'email'     => $email,
                'password'  => md5($pass),
                'address'   => $address,
                'username'  => $username,
                'phone'     => $phone,
                'created'   => date('Y-m-d H:i:s')
            );
            return $this->db->insert('accounts', $data);
        }else{
            return FALSE;
        }
    }
    /*
    *Check email in database
    * 
    */
    public function checkEmailM($email){
        if(isset($email)){
            $this->db->having('email=',$email);
            $this->db->where('deleted=');
            return $this->db->get('accounts');
        }else{
            return FALSE;
        }
    }
    /*
    *Search account with data and type (email or username)
    * 
    */
    public function searchAccount($search, $radio)
    {
        $type=[
            '0' => 'username',
            '1' => 'email'
        ];
        $query=$this->db->select('accounts.id as accid, accounts.email, accounts.address, accounts.username, accounts.phone,roles.role_name ')
        ->join('roles','roles.id=accounts.role_id')
        ->where('accounts.deleted=')
        ->like($type["$radio"], $search)
        ->get('accounts');
        return $query->result_array();
    }
}