<?php 
namespace CI316\core;
abstract class BaseController extends \CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('session'));
        $nonCheck=[
            'showLogin',
            'actionLogin',
            'showRegister',
            'actionRegister'
        ];
        $method = $this->router->fetch_method();
        if(uri_string()!='login'&&uri_string()!='register'){
            $uri = ['last_page'  => uri_string()];
            $this->session->set_userdata(['last_page'  => uri_string()]);
        }
        $last_page=$this->session->userdata('last_page');
        $account = $this->session->userdata('email');
        if (empty($account) && !in_array($method, $nonCheck)) {
            redirect("/login");
        }
        if (!empty($account) && in_array($method, $nonCheck)) {
            isset($last_page) ? redirect($last_page) :redirect("/home");
        }
    }
    /*
    * Hash code with key
    * input: string, action
    * output: action = e -> return encode string |  action = d -> return decode string
    */
    public function my_crypt( $string, $action = 'e' ) {
        $secret_key = 'my_secret_key';
        $secret_iv = 'my_secret_iv';
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
        return $output;
    }
}