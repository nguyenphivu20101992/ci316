<?php


use Phinx\Seed\AbstractSeed;

class AddRowsRoles extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'            => '1',
                'role_name'     => 'Admin'
            ],
            [
                'id'            => '0',
                'role_name'     => 'User',
            ]
        ];

        // this is a handy shortcut
        $this->insert('roles', $rows);        
    }
}
